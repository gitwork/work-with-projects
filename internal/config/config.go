package pkg

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

type Config struct {
	BaseUrl string `yaml:"base_url"`
	Token   string `yaml:"token"`
}

func CreateConfig() *Config {
	f, err := os.ReadFile("configs/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	var c Config
	if err := yaml.Unmarshal(f, &c); err != nil {
		log.Fatal(err)
	}
	return &c
}
