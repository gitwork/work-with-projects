package pkg

import (
	"errors"
	"github.com/xanzy/go-gitlab"
)

type AccessLevelString string

const (
	Guest      AccessLevelString = "Guest"
	Reporter   AccessLevelString = "Reporter"
	Developer  AccessLevelString = "Developer"
	Maintainer AccessLevelString = "Maintainer"
)

func ConvertBytes(size int64) (int64, string) {
	switch {
	case size/1024 == 0:
		return size, "b"
	case size/1024/1024 == 0:
		return size / 1024, "Kb"
	case size/1024/1024/1024 == 0:
		return size / 1024 / 1024, "Mb"
	}
	return size, "b"
}

func RoleMapping(role string) (int, error) {
	switch {
	case role == string(Guest):
		return int(gitlab.GuestPermissions), nil
	case role == string(Reporter):
		return int(gitlab.ReporterPermissions), nil
	case role == string(Developer):
		return int(gitlab.DeveloperPermissions), nil
	case role == string(Maintainer):
		return int(gitlab.MaintainerPermissions), nil
	}
	return -1, errors.New("role doesnt exist")
}

func DateProcessing(date []string) string {
	if date == nil {
		return ""
	}
	return date[0]
}
