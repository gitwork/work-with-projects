package pkg

import (
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	config "gitlab.com/gitwork/work-with-projects/internal/config"
	"time"
)

func GitInit(config *config.Config) (*gitlab.Client, error) {
	git, err := gitlab.NewClient(config.Token, gitlab.WithBaseURL(config.BaseUrl))
	if err != nil {
		logrus.Warnf("Error connect to %s with token %s", config.BaseUrl, config.Token)
		return git, err
	}
	logrus.Infoln("Connect to %s with token %s", config.BaseUrl, config.Token)
	return git, err
}

func GetGroupByName(git *gitlab.Client, groupName string) (*gitlab.Group, error) {
	group, _, err := git.Groups.GetGroup(groupName, &gitlab.GetGroupOptions{})
	if err != nil {
		logrus.Infof("Group %s not found", groupName)
		return group, err
	}
	logrus.Infof("Group %s found!", groupName)
	return group, err
}

func GetProjectFromGroup(git *gitlab.Client, pathProject string) (*gitlab.Project, error) {
	project, _, err := git.Projects.GetProject(pathProject, &gitlab.GetProjectOptions{Statistics: gitlab.Ptr(true)})
	if err != nil {
		logrus.Infof("Project %s not found", pathProject)
		return project, err
	}
	logrus.Infof("Project %s found", pathProject)
	return project, err
}

func GetUsersFromProject(git *gitlab.Client, pathProject string) ([]*gitlab.ProjectMember, error) {
	users, _, err := git.ProjectMembers.ListAllProjectMembers(pathProject, &gitlab.ListProjectMembersOptions{})
	if err != nil {
		logrus.Infof("Get Users error.Project %s not found", pathProject)
		return users, err
	}
	logrus.Infof("Get Users from Project %s success", pathProject)
	return users, err
}

func SearchUserInProject(git *gitlab.Client, pathProject string, username string) (*gitlab.User, error) {
	user, _, err := git.Search.UsersByProject(pathProject, username, &gitlab.SearchOptions{})
	if err != nil {
		logrus.Infof("Search User In Project Error. In project: %s User: %s", pathProject, username)
		return &gitlab.User{}, err
	}
	logrus.Infof("Search User In Project Success.User %s in Project %s", username, pathProject)
	return user[0], err

}

func CheckProjectLimits(git *gitlab.Client, pathProject string, sizeLimit int64, capacityLimit string, usersCountLimit int) (bool, error) {
	project, err := GetProjectFromGroup(git, pathProject)
	if err != nil {
		logrus.Infof("Project %s not found", pathProject)
		return false, err
	}
	sizeProject, capacityProject := ConvertBytes(project.Statistics.StorageSize)
	users, err := GetUsersFromProject(git, pathProject)
	if err != nil {
		logrus.Infof("Error get users from project:%s", pathProject)
		return false, err
	}
	usersCount := len(users)
	if capacityProject != capacityLimit || sizeProject > sizeLimit || usersCount > usersCountLimit {
		return false, err
	}
	return true, err
}

func ChangeRoleUserProject(git *gitlab.Client, pathProject string, username string, newRole string, expiresAt ...string) (*gitlab.ProjectMember, error) {
	user, err := SearchUserInProject(git, pathProject, username)
	if err != nil {
		logrus.Infof("Сannot change(search user) the role for a user: %s in Project: %s", username, pathProject)
		return &gitlab.ProjectMember{}, err
	}
	role, err := RoleMapping(newRole)
	if err != nil {
		logrus.Infof("Role:%s doesnt exist", newRole)
		return &gitlab.ProjectMember{}, err
	}
	date := DateProcessing(expiresAt)
	userWithNewRole, _, err := git.ProjectMembers.EditProjectMember(pathProject, user.ID, &gitlab.EditProjectMemberOptions{AccessLevel: (*gitlab.AccessLevelValue)(gitlab.Ptr(role)), ExpiresAt: gitlab.Ptr(date)})
	if err != nil {
		logrus.Infof("Failed to change role:%s for user:%s", newRole, username)
		return &gitlab.ProjectMember{}, err
	}
	return userWithNewRole, err
}

func GetUserActivity(git *gitlab.Client, pathProject string, username string) (string, error) {
	commits, _, err := git.Commits.ListCommits(pathProject, &gitlab.ListCommitsOptions{Author: gitlab.Ptr(username), All: gitlab.Ptr(true)})
	if err != nil {
		logrus.Infof("Error getting commit in Project: %s for User: %s", pathProject, username)
		return "", err
	}
	if commits != nil {
		lastCommit := commits[len(commits)-1]
		lastCommitDate := time.Time.String(*lastCommit.CommittedDate)
		return lastCommitDate, err
	}
	return "", err
}
